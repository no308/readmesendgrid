<img src="../../_resources/twilio_sendgrid_logo.png" alt="twilio_sendgrid_logo.png" width="500" height="250">

# buying a domain name

<img src="../../_resources/1.png" alt="1.png" width="699" height="333">

# creating the app service domain

<img src="../../_resources/2.png" alt="2.png" width="700" height="501">

# then full all of the important informations

<img src="../../_resources/3.png" alt="3.png" width="700" height="518">

##### and then create the app service domain

##### after creating the domain it will show us this page so we have to click on manage DNS records

<img src="../../_resources/5.png" alt="5.png" width="702" height="401">

##### after that it will show us the follwing page so we have to click on Record set

<img src="../../_resources/6.png" alt="6.png" width="701" height="389">

##### and to use this we have to link it with another service called **Twilio SendGrid** you can find it in azure marketplace

<img src="../../_resources/7.png" alt="7.png" width="702" height="589">

### then go to the resource group and open SaaS Account on publisher’s site and connect with your azure acount to show sendgrid page

<img src="../../_resources/8.png" alt="8.png" width="698" height="328">

#### go to the settind

<img src="../../_resources/9.png" alt="9.png" width="703" height="347">

### and then choose Sender Authentication

<img src="../../_resources/10.png" alt="10.png" width="701" height="363">

### choose then authenticate Your Domain

<img src="../../_resources/11.png" alt="11.png" width="700" height="325">

### for the DNS host select other host

### and for which DNS host you have to go back to the app service domain and to take the domain name which you have created it and put here

#### after that it will show for us the following page so what we have to do is to copy all of the host and values and put it in the app service domain all of them

<img src="../../_resources/12.png" alt="12.png" width="700" height="309">

### click on Record set and then copy the host from sendgrid in the name box in the type you have to choose CNAME in the ALIAS box put the value of the sendgrid sender authentication repeat this three times with all of the host and the values and then click ok and that's it

<img src="../../_resources/13.png" alt="13.png" width="700" height="348">
