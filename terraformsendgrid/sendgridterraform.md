<img src="../../_resources/twilio_sendgrid_logo.png" alt="twilio_sendgrid_logo" width="500" height="200">

# Documentation sendgrid
## Sommaire

> - Objectifs
> - Graph
> - providers
> - Ressources
> - Get it started

* * *

## Objectif

- Create a domain name and sendgrid service
    - useing the domain name
    - using the API

* * *

## Dependency graph

<img src="../../_resources/sendgrid.png" alt="sendgrid.png" width="699" height="333">
* * *

## Definition of providers

- azurerm = required_providers
    
    - source = "hashicorp/azurerm"
    - version = ">= 3.23.0"
        -sendgrid
    - source = "Trois-Six/sendgrid"
    - version = "= 0.2.1"

## Common resources

- resource "azurerm\_resource\_group" "p20cloud" → creation the resource group
- resource "azurerm\_dns\_zone" "p20cloud" → creation the dns zone
- resource "sendgrid\_domain\_authentication" "p20cloud" →authentication the domain name
- resource "azurerm\_dns\_txt_record" "p20cloud" →Automated Security
- resource "azurerm\_dns\_mx_record" "p20cloud" →Automated Security

* * *

## Get it started

```bash
terraform init
terraform plan
terraform apply --auto-approve
```
